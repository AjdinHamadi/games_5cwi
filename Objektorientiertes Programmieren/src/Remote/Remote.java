package Remote;

public class Remote {
	private boolean isOn;
	private boolean hasPower;
	private Battery battery1;
	private Battery battery2;
	
	public Remote(boolean isOn, boolean hasPower, Battery battery1, Battery battery2) {
		super();
		this.isOn = isOn;
		this.hasPower = hasPower;
		this.battery1 = battery1;
		this.battery2 = battery2;
	}

	public void turnOn() {
		this.isOn = true;
	}

	public void turnOff() {
		this.isOn = false;;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public int getStatus() {
		int getstatus;
		getstatus = (battery1.getChargingStatus() + battery2.getChargingStatus()) / 2;
		return getstatus;
	}
	
}
