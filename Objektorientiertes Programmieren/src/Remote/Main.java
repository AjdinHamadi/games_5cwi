package Remote;

public class Main {
	
		public static void main(String[] args) {
			
			Battery b1 = new Battery(10);
			Battery b2 = new Battery(35);
			
			Remote r1 = new Remote(true,true,b1,b2);
			System.out.println(r1);
			
			System.out.println(r1.isOn());
			System.out.println(r1.getStatus());
			r1.turnOff();
			System.out.println(r1.isOn());
			
		}
}
