public class test {
  
  public static void main(String[] args) {
	Engine e1 = new Engine(150, "diesel");
	Engine e2 = new Engine(460, "benzin");
	
    Car c1 = new Car("yellow", e1);
    Car c2 = new Car("black", e2);
    
    System.out.println(c1.getEngine());
    
  }
}