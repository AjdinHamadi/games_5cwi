package Car_Sample;

public class Car {
	private String color;
	private double maxkmh;
	private double basicprice;
	private double usage;
	private Engine engine;
	private Producer producer;
	private double distance;
	

	public Car(String color, double maxkmh, double basicprice, double usage, Engine engine, Producer producer, double distance) {
		super();
		this.color = color;
		this.maxkmh = maxkmh;
		this.basicprice = basicprice;
		this.usage = usage;
		this.engine = engine;
		this.producer = producer;
		this.distance = distance;

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getMaxkmh() {
		return maxkmh;
	}

	public void setMaxkmh(double maxkmh) {
		this.maxkmh = maxkmh;
	}

	public double getbasicprice() {
		return basicprice;
	}

	public void setbasicprice(double basicprice) {
		this.basicprice = basicprice;
	}

	public double getUsage() {
		if(distance >= 50000) {
			usage = usage*1.098;
			return usage;
		}
		else {
			return usage;
		}
	}

	public void setUsage(double usage) {
		this.usage = usage;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public double price() {
		double price;
		price = basicprice * (1.00-producer.getDiscount());
		return price; 
	}

}

