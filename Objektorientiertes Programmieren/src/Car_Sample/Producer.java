package Car_Sample;

public class Producer {
	private String Name;
	private String Location;
	private double discount;
	
	public Producer(String name, String location, double discount) {
		super();
		Name = name;
		Location = location;
		this.discount = discount;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public double getDiscount() {
		discount = discount * 0.01;
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
}	
	



	