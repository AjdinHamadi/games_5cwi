package Car_Sample;

public class Engine {
	private int ps;
	private String oil;
	
	public Engine(int ps, String oil) {
		super();
		this.ps = ps;
		this.oil = oil;
	}
	public int getPs() {
		return ps;
	}
	public void setPs(int ps) {
		this.ps = ps;
	}
	public String getOil() {
		return oil;
	}
	public void setOil(String oil) {
		this.oil = oil;
	} 
	
}

