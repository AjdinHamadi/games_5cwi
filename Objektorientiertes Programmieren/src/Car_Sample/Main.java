package Car_Sample;

public class Main {

	public static void main(String[] args) {
		Engine e1 = new Engine(400, "diesel");
		Engine e2 = new Engine(780, "benzin");
		
		Producer p1 = new Producer("BMW", "Germany", 10);
		Producer p2 = new Producer("Audi", "Swiss", 20);
		
		Car c1 = new Car("gold", 350, 110000, 8, e2, p1,70000);
		Car c2 = new Car("grey", 260, 70000, 6, e1, p2,23000);
		
		System.out.println(c1.getbasicprice());
		System.out.println(c1.price());
		System.out.println(c1.getEngine().getOil());
		System.out.println(c1.getColor());
		System.out.println(c1.getProducer().getName());
		System.out.println(c1.getUsage());
		
	}
}

