
public class Engine {
	private int PS;
	private String type;
	
	public Engine(int PS, String type) {
		super();
		this.PS = PS;
		this.type = type;
	} //Diesel, Benzin, ...

	public int getPS() {
		return PS;
	}

	public void setPS(int PS) {
		this.PS = PS;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
