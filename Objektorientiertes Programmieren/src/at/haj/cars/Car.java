package at.haj.cars;

public class Car extends Vehicle {
	private int amountofTires;
	private double discount;


	public Car(String name, String color, double price, int amountofTires, double discount) {
		super(name, color, price);
		this.amountofTires = amountofTires;
		this.discount = discount;
	}

	public int getAmountofTires() {
		return amountofTires;
	}

	public void setAmountofTires(int amountofTires) {
		this.amountofTires = amountofTires;
	} 
	
	public double getPrice() {
		double fee;
		fee = this.price * discount;
		return fee;
	}
	
}
