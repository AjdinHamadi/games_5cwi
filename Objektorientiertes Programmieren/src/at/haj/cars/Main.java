package at.haj.cars;

public class Main {

	public static void main(String[] args) {
		
			Vehicle v1 = new Car("BMW", "blue", 104500, 4, 0.2);
			Vehicle v2 = new Boat("Superboat", "white", "Propeller1", 0.5, 70590);
			
			System.out.println(v1.getName());
			System.out.println(v1.getColor());
			System.out.println(v1.getPrice());
	}
}
