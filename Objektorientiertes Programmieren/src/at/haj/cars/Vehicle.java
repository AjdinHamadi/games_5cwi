package at.haj.cars;

public class Vehicle {
	private String name;
	private String color;
	protected double price;
	
	public Vehicle(String name, String color, double price) {
		super();
		this.name = name;
		this.color = color;
		this.price = price; 
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
