package at.haj.cars;

public class Boat extends Vehicle {
	private String propeller;
	private double discount;

	public Boat(String name, String color, String propeller, double discount, double price) {
		super(name, color, price);
		this.propeller = propeller;
		this.discount = discount;
	}

	public String getPropeller() {
		return propeller;
	}

	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
	
	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getPrice() {
		double fee;
		fee = this.price * discount;
		return fee;
	}
	
}
