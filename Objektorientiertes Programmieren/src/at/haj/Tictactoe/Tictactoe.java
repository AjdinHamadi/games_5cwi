package at.haj.Tictactoe;

import java.util.Scanner;

public class Tictactoe {

	 char[][] field = new char[3][3];
	 int feld = 0;
	 Scanner action = new Scanner(System.in);
	 boolean winner = false;
	 char player = 'X';
	
	 public void initField() {
	  for (int row = 0; row < 3; row++) {
	   for (int col = 0; col < 3; col++) {
	    field[row][col] = 0;
   }
  }
 }

	 public String[] setSign() {
	  String Storage = action.next();
	  String[] storageArr = Storage.split(",");
	  return storageArr;
 }

	 private boolean addInputToBoard(String[] input) {
	  int column = Integer.parseInt(input[0]);
	  int row = Integer.parseInt(input[1]);
	  if (field[row][column] > 0) {
	   System.out.println("Dieses Feld ist schon besetzt!");
	   return false;
	  } else {
	   field[row][column] = player;
	   return true;
	  }

	 }
	 private void switchPlayer() {
	  if (player == 'X') {
	   player = 'O';
	  } else if (player == 'O') {
	   player = 'X';
	  }
	 }
	 private boolean CheckForAWinner() {
	  boolean Winner = false;
	  for (int row = 0; row <= 2; row++) {
	   if (field[row][0] == field[row][1] && field[row][1] == field[row][2] && field[row][0] > 0 || field[0][row] == field[1][row] && field[1][row] == field[2][row] && field[0][row] > 0) {
	    Winner = true;
	   }
	  }
	  if (field[0][0] == field[1][1] && field[1][1] == field[2][2] && field[0][0] > 0) {
	   Winner = true;
	  }
	  if (field[0][2] == field[1][1] && field[1][1] == field[2][0] && field[0][2] > 0) {
	   Winner = true;
	  }
	  return Winner;
	 }
	
	 private void print() {
	  System.out.println(field[0][0] + " | " + field[0][1] + " | " + field[0][2]);
	  System.out.println(field[1][0] + " | " + field[1][1] + " | " + field[1][2]);
	  System.out.println(field[2][0] + " | " + field[2][1] + " | " + field[2][2]);
	 }
	
	 public void go() {
	  initField();
	  while (!winner){
	   print();
	   String[] input = setSign();
	   boolean set = addInputToBoard(input);
	   if (set) {
	    boolean won = CheckForAWinner();
	    if (won) {
	     print();
	     System.out.println("Spieler " + player + " hat gewonnen!");
	     winner = true;
	    }
	    switchPlayer();
	   }
	  }
	 }

}