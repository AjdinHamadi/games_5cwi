package at.haj.encryptor;

public interface Encryptor {
	public String encrypt(String str);
	public String decrypt(String str);
}
