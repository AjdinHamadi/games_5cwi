package at.haj.encryptor;

public class Decryptor implements Encryptor{

	@Override
	public String encrypt(String str) {
		System.out.println("Encrypte das Wort: " + str);
		for (int i = 0;i < str.length(); i++){
		    int charValue = str.charAt(i);
		    String letter = String.valueOf( (char) (charValue + 2));
		    System.out.println(letter);
		}
		return str;
	}

	@Override
	public String decrypt(String str) {
		System.out.println("Decrypte das Wort: " + str);
		for (int i = 0;i < str.length(); i++){
		    int charValue = str.charAt(i);
		    String letter = String.valueOf( (char) (charValue - 2));
		    System.out.println(letter);
		}
		return str;
	}

}
