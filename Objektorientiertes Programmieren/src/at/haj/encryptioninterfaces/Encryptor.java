package at.haj.encryptioninterfaces;

public interface Encryptor {
	public String encrypt();
	public String getFounder();
	
}
