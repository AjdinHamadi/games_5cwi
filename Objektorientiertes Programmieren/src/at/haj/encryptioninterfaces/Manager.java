package at.haj.encryptioninterfaces;

import java.util.ArrayList;
import java.util.List;

public class Manager {
	private List<Encryptor> encryptors; 
	
	public Manager() {
		this.encryptors = new ArrayList<Encryptor>();
	}
	
	public void setEncryptor() {
		for (Encryptor e : encryptors) {
			e.encrypt();
		}
	}
	
	public void doEncrypt(Encryptor e) {
		this.encryptors.add(e);
	}
	
}
