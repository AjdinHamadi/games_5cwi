package at.haj.encryptioninterfaces;

public class Main {
	public static void main(String[] args) {
		Manager manager = new Manager();
		
		Cesar c1 = new Cesar("Albert");
		Algo1 a1 = new Algo1("Julian");
		Algo2 a2 = new Algo2("J�rgen");
		
		manager.doEncrypt(c1);
		manager.doEncrypt(a1);
		manager.doEncrypt(a2);
		System.out.println(a1.encrypt());

	}
}
