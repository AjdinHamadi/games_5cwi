package at.haj.interfaces;

public class Main {
	public static void main(String[] args) {
		Player player = new Player();
		
		Title t1 = new Title();
		Item i1 = new Item();
		Song s1 = new Song();
		
		player.addPlayable(t1);
		player.addPlayable(i1);
		player.addPlayable(s1);
	
		player.playAll();
		
	}
}
