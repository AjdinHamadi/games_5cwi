package at.haj.interfaces;

public interface Playable {
	public void play();
}
