package at.haj.observerpattern;

public interface Observable {
	public void inform();
}
