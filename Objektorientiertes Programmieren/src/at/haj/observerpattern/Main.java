package at.haj.observerpattern;

public class Main {
	
	public static void main(String[] args) {
		Observable c1 = new Christmas();
		Observable l1 = new Lantern();
		Observable t1 = new Lantern();
		
		Sensor s1 = new Sensor();
		
		s1.addObservable(c1);
		s1.addObservable(l1);
		s1.addObservable(t1);
		
		s1.informAll();
	}
}
